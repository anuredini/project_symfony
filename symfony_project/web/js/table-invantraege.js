
/* Initialise the table with the required column ordering data types */
function addButton(user){
    if ( user ) {
        return {text: 'Antrag hinzufügen',action: function ( e, dt, node, config ) {showModaladdInvantrag();}};
    }
}

//var user = "admin";

/*
buttons = [ { extend: 'copy', text: 'Kopieren' } ];
buttons.unshift('copy');
*/

function initTable(user){
    //init datatable
    $('#tableInvestitionsantraege').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'pageLength',
                { extend: 'copy', text: 'Kopieren' },
                { extend: 'csv', text: 'CSV' },
                { extend: 'excel', text: 'Excel' },
                { extend: 'pdf', text: 'PDF', title: 'title' },
                { extend: 'print', text: 'Drucken' },
                addButton(user)
            ],
            "language": {
                "sEmptyTable":      "Keine Daten in der Tabelle vorhanden",
                "sInfo":            "_START_ bis _END_ von _TOTAL_ Einträgen",
                "sInfoEmpty":       "0 bis 0 von 0 Einträgen",
                "sInfoFiltered":    "(gefiltert von _MAX_ Einträgen)",
                "sInfoPostFix":     "",
                "sInfoThousands":   ".",
                "sLengthMenu":      "_MENU_ Einträge anzeigen",
                "sLoadingRecords":  "Wird geladen...",
                "sProcessing":      "Bitte warten...",
                "sSearch":          "Suchen",
                "sZeroRecords":     "Keine Einträge vorhanden.",
                "oPaginate": {
                    "sFirst":       "Erste",
                    "sPrevious":    "Zurück",
                    "sNext":        "Nächste",
                    "sLast":        "Letzte"
                },
                "oAria": {
                    "sSortAscending":  ": aktivieren, um Spalte aufsteigend zu sortieren",
                    "sSortDescending": ": aktivieren, um Spalte absteigend zu sortieren"
                },
                buttons: {
                    pageLength: 'Einträge'
                }
            },
            "columns": [ //all column type here. null if is normal text
                { "orderDataType": "dom-dom-text-numeric" },
                null,
                null,
                null,
                null,
                null,
                null,
                { "orderable": false }
                /*
                { "orderDataType": "dom-text", type: 'string' },
                { "orderDataType": "dom-text-numeric" },
                { "orderDataType": "dom-select" },
                { "orderDataType": "dom-checkbox" }
                */
            ]
    } );

    //init datepicker
    $.fn.datepicker.dates['de'] = {
        days:["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"],daysShort:["Son","Mon","Die","Mit","Don","Fre","Sam"],daysMin:["So","Mo","Di","Mi","Do","Fr","Sa"],months:["Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember"],monthsShort:["Jan","Feb","Mär","Apr","Mai","Jun","Jul","Aug","Sep","Okt","Nov","Dez"],today:"Heute",monthsTitle:"Monate",clear:"Löschen",weekStart:1,format:"dd.mm.yyyy"
    }
    $('#datetimepicker2').datepicker({
        todayHighlight: true,
        autoclose: true,
        language: 'de'
    });
}



