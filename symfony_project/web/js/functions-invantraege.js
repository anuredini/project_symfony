

//show modal addinvestantrag
function showModaladdInvantrag(){
    //init fields modal
    $("#gesamtsumme").val("0");
    $("#textArea").val("");
    $("#date").val("");
    
    $("#last_row input").each( function () {
        $(this).removeClass("error_on_input");
    });					

    $('div #last_row').children().each(function() {
        if ( $(this).is(':first-child') ){
            $(this).find("input").not(':last').each(function() {
                $(this).val("");
            });
            $("#gesamt").val("0");
        }
        else{
            $(this).remove();
        }
        
    });

    ($("#last_row").find("div:not(:first-child) input")).on( 'keyup', setSum);

    $('#addInvantragModal').modal({show:true});
}

//show modal viewinvestantrag
function showModalviewInvantrag(obj){
    //init fields modal

    $(".format2").each( function () {
        $(this).remove();
    });

    $(".to_erase").each( function () {
        $(this).text("").removeAttr( 'style' );
    });
    
    var id = $(obj).val();
    var children = $("#tableInvestitionsantraege tbody").find('tr[val="'+ id + '"]');
    
    $( "#antrag_nummer" ).text(children.find("td:eq( 0 )").text());
    $( "#view_antragsteller" ).text(children.find("td:eq( 1 )").text());
    $( "#view_kostenstelle" ).text(children.find("td:eq( 2 )").text());
    $( "#view_budget" ).text(children.find("td:eq( 3 )").text());
    $( "#view_datum" ).text(children.find("td:eq( 4 )").text());
    $( "#view_gesamtsumme" ).val(children.find("td:eq( 5 )").text());
    $( "#view_invantrag" ).val(id);
    $('#view_form').attr('action', '/investantrag/'+id);

    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        url: "/getdateninvestantrag",
        data: { id : id },
        cache: false,
        dataType: "json",
        success: function(result){
            //the form has successed
            //alert(result.best[0].position);
            if(result.res){
                //antrag ansehen erlaubt
                $( "#view_begr" ).val(result.begruendung);
                //alert(result.daten);
                //get bestätigungen
                for(var i = 0; i < (result.best).length; i++){
                    if(result.best[i].position == 2){
                        $( "#view_kst" ).text(result.best[i].datum+", "+result.best[i].bestaetiger).css('border-bottom','1px groove black');
                    }
                    else if(result.best[i].position == 3){
                        $( "#view_b" ).text(result.best[i].datum+", "+result.best[i].bestaetiger).css('border-bottom','1px groove black');
                    }
                    else if(result.best[i].position == 4){
                        $( "#view_f" ).text(result.best[i].datum+", "+result.best[i].bestaetiger).css('border-bottom','1px groove black');	
                    }
                    else if(result.best[i].position == 5){
                        $( "#view_g" ).text(result.best[i].datum+", "+result.best[i].bestaetiger).css('border-bottom','1px groove black');
                    }								
                }
                //get daten
                for(var i = 0; i <= (result.daten).length; i++){
                    if (i>0 && (i % 4 == 0)) {
                        var bezeichnung = result.daten[i-4];
                        var menge = result.daten[i-3];
                        var ep = result.daten[i-2];
                        var summe = result.daten[i-1];
                        var $row2 = $('<div class="row format2"><div class="col-md-5">'+bezeichnung+'</div><div class="col-md-2">'+menge+'</div><div class="col-md-2">'+ep+'</div><div class="col-md-2">'+summe+'</div></div>');
                        $row2.attr('style', 'margin-top:5px');
                        $row2.appendTo($("#view_daten"));
                    }
                    
                }
                $('#viewInvantragModal').modal({show:true});
            }else{
                alert("Nicht erlaubt!");
            }
        }
    });

}


//calculations
function setSum(e){
    //init variables
    finalTot   = parseFloat($("#gesamtsumme").val()); //init sum
    
    tmp_menge  = $(this).parent().parent().find("div:nth-child(2) input");
    tmp_ep     = $(this).parent().parent().find("div:nth-child(3) input");
    tmp_gesamt = $(this).parent().parent().find("div:nth-child(4) input");
    
    //console.log(tmp_menge.val()+", "+tmp_ep.val()+", "+tmp_gesamt.val()+", "+($("#gesamtsumme").val()));
    
    if(!isNaN(tmp_gesamt.val()))//only number
        finalTot -= parseFloat(tmp_gesamt.val()); //remove the old partial sum

    tmp_gesamt.val((tmp_menge.val() * tmp_ep.val()).toFixed(2)); //set the new partial sum

    if(!isNaN(tmp_gesamt.val()))//only number
        finalTot += parseFloat(tmp_gesamt.val()); //add the new partial sum

    $("#gesamtsumme").val(finalTot.toFixed(2)); //set the final total sum
}



//add row info on modal antrag
function addInfoAntrag(){
    var $row = $('<div class="row format"><div class="col-md-5"><input size="40" type="text" name="bezeichnung" value=""/></div><div class="col-md-2"><input size="5" type="text" name="menge" value=""/></div><div class="col-md-2"><input size="10" type="text" name="ep" value=""/></div><div class="col-md-2"><input style="border:none; background-color: transparent;" size="13" type="text" name="summe" value="0" readonly/></div><a href="#"><div onclick="deleteInfoAntrag(this)" id="remove_row" class="remove_row col-md-1 glyphicon glyphicon-minus "></div></a></div>');
    $row.attr('style', 'margin-top:5px');
    $row.appendTo($("#last_row"));
    ($row.find("div:not(:first-child) input")).on( 'keyup', setSum);
}

//delete row info on modal antrag
function deleteInfoAntrag(obj){
    var sum_par = $(obj).parent().parent().find("div:nth-child(4) input").val();
    if(!isNaN(sum_par))
        $("#gesamtsumme").val(($("#gesamtsumme").val() - sum_par).toFixed(2));
        $(obj).parent().parent().remove();
}


//get values from selected row
function getEditValues(id, textOption){
    $(id).children().each( function () {
        var rechte_text = $(this).text();
        if(rechte_text == textOption){
            valOption = $(this).val();
        }
    });
    return valOption;
}

function setButtons(userRechte, investantrag){
    if(userRechte)
        return '<td><button onclick="deleteInvestantrag(this)" title="Antrag löschen" style="color: #a94442;background-color: transparent;" type="button" class="del btn btn-default glyphicon glyphicon-trash" value='+investantrag+'></button> <button onclick="showModalviewInvantrag(this)" title="Antrag ansehen" style="color: #a94442;background-color: transparent;" type="button" class="view btn btn-default glyphicon glyphicon-eye-open" value='+investantrag+'></button></td>'
    else{
        return '<button onclick="showModalviewInvantrag(this)" title="Antrag ansehen" style="color: #a94442;background-color: transparent;" type="button" class="view btn btn-default glyphicon glyphicon-eye-open" value='+investantrag+'></button></td>'
    }
}

//Antrag stellen
function addInvantrag(userRechte, benutzer_id, benutzer_name, benutzer_position){
    var fehler = false;
    var children = $('div#last_row input').not('input[name="gesamt[]"]');
    
    for(var i = 0;i < children.length;i++){
        if(children.eq(i).val() == ""){
            children.eq(i).addClass("error_on_input");
            fehler = true;
        }
    }
    
    if($("#date").val() == ""){
        fehler = true;
        $("#date").addClass("error_on_input");
    }
    
    else{
        $("#date").removeClass("error_on_input");
    }
    
    if (fehler == false) {
        //alert("kein fehler");
        var kostenstelle = antrag_form.elements["kostenstelle"].value;
        var kostenstelle_text = $('#kostenstelle_add').find('option[value='+ kostenstelle + ']').text();
        var datum = $("#date").val();
        var budget = $("#budget").val();
        var begruendung = antrag_form.elements["textArea"].value;
        var gesamtsumme = $("#gesamtsumme").val();
        
        var info = {};
        
        var children = $('div#last_row input');

        for(var i = 0;i < children.length;i++){
            name = children.eq(i).attr( "name" );
            value = children.eq(i).val();
            info[i] = value;
        }
        
        var jsonString = JSON.stringify(info);
        
        //send to ajax
        $.ajax({
            type: "POST",
            url: "/addinvestantrag",
            data: { info_array : jsonString, benutzer_id, kostenstelle, datum, budget, begruendung, gesamtsumme, benutzer_position},
            cache: false,
            //dataType: "json",
            success: function(result){
                // the form has successed
                var t = $('#tableInvestitionsantraege').DataTable();
                var values = $.parseJSON(result);

                if(values.noerror){
                    alert("Antrag hinzugefügt!");
                    //alert("Email an: "+values.naechste_benutzer);
                    $('#addInvantragModal').modal('hide');
                    var budget_text;
                    if(budget==1){
                        budget_text = "Ja";
                    }
                    else{
                        budget_text = "Nein";
                    }
                    
                    var rowNode = t
                        .row.add( [ 
                        values.id_name,
                        benutzer_name,
                        kostenstelle_text,
                        budget_text,
                        datum,
                        gesamtsumme,
                        values.status,
                        setButtons(userRechte, values.id_invantrag)
                        ] ).draw();
                    
                    t.rows(rowNode).nodes().to$().attr("val", values.id_invantrag);
                }
                else{
                    alert("Fehler bei der Antragstellung!");
                }
                
            }
        });
        
        /*
        ($("#last_row").find("div:nth-child(4) input")).each( function () {
            var sum_par = $(this).val();
            if(!isNaN(sum_par))
            $("#gesamtsumme").val(($("#gesamtsumme").val() - sum_par).toFixed(2));
        });					
        
        $("#gesamt").val("0");
        $("#textArea").val("");
        $("#date").val("");
        
        $("#last_row input").each( function () {
            $(this).removeClass("error_on_input");
        });					

        $('div#last_row').children().each(function() {
            
            if ( $(this).is(':first-child') ){
                
                $(this).find("input").not(':last').each(function() {
                    $(this).val("");
                });
                
            }
            else{
                $(this).remove();
            }
            
        });
        */

    }
    else{
        alert("Überprüfe die Daten!");
    }

}


function showModaleditKostenstelle(obj){
    //id_edit = $(obj).val();
    tr_selected = $(obj).parents('tr');

    // var children = $("tbody").find('button[value="'+ $(this).val() + '"]').parent().parent();

    myForm2.elements["kostenstelle_edit"].value = tr_selected.find("td:eq( 0 )").text();
    myForm2.elements["bezeichnung_edit"].value = tr_selected.find("td:eq( 1 )").text();
    
    var kst_leiter_edit = tr_selected.find("td:eq( 2 )").text();
    var stellvertreter_edit = tr_selected.find("td:eq( 3 )").text();
    var bereichsleiter_edit = tr_selected.find("td:eq( 4 )").text();
    
    // set selected values
    $('#kst_leiter_edit').val(getEditValues("#kst_leiter_edit", kst_leiter_edit));
    $('#stellvertreter_edit').val(getEditValues("#stellvertreter_edit", stellvertreter_edit));
    $('#bereichsleiter_edit').val(getEditValues("#bereichsleiter_edit", bereichsleiter_edit));
    
    $('#editKostenstelleModal').modal({show:true});

}

function deleteInvestantrag(obj){
    if(confirm('Antrag löschen?')) {
        var table = $('#tableInvestitionsantraege').DataTable();
        var tr_selected = $(obj).parents('tr');	
        var id = $(obj).val();
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/deleteinvestantrag",
            data: { id },
            //cache: false,
            success: function(result){
                if(result){
                    alert("Antrag gelöscht!");
                    // the form has successed
                    // lösche die Zeile
                    table
                            .row( tr_selected )
                            .remove()
                            .draw();
                }
                else{
                    alert("Fehler beim Löschen!");
                }
            }
        });
    }
}

