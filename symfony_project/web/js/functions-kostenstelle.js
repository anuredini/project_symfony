
// show modal
function showModaladdKostenstelle(){
    $('#addKostenstelleModal').modal({show:true});
    $('#kostenstelle_add').val("");
	$('#bezeichnung_add').val("");
    $('#kst_leiter_add').val("wahl");
    $('#stellvertreter_add').val("wahl");
    $('#bereichsleiter_add').val("wahl");
}

// get values from selected row
function getEditValues(id, textOption){
    $(id).children().each( function () {
        var rechte_text = $(this).text();
        if(rechte_text == textOption){
            valOption = $(this).val();
        }
    });
    return valOption;
}

var tr_selected;

function showModaleditKostenstelle(obj){
    //id_edit = $(obj).val();
    tr_selected = $(obj).parents('tr');

    // var children = $("tbody").find('button[value="'+ $(this).val() + '"]').parent().parent();

    myForm2.elements["kostenstelle_edit"].value = tr_selected.find("td:eq( 0 )").text();
    myForm2.elements["bezeichnung_edit"].value = tr_selected.find("td:eq( 1 )").text();
    
    var kst_leiter_edit = tr_selected.find("td:eq( 2 )").text();
    var stellvertreter_edit = tr_selected.find("td:eq( 3 )").text();
    var bereichsleiter_edit = tr_selected.find("td:eq( 4 )").text();
    
    // set selected values
    $('#kst_leiter_edit').val(getEditValues("#kst_leiter_edit", kst_leiter_edit));
    $('#stellvertreter_edit').val(getEditValues("#stellvertreter_edit", stellvertreter_edit));
    $('#bereichsleiter_edit').val(getEditValues("#bereichsleiter_edit", bereichsleiter_edit));
    
    $('#editKostenstelleModal').modal({show:true});
}

function setButtons(user, kostenstelle){
    if (user) {
        return '<button onclick="deleteKostenstelle(this)" title="Kostenstelle löschen" style="color: #a94442;background-color: transparent;" type="button" class="val btn btn-default glyphicon glyphicon-trash" value='+kostenstelle+'></button> <button onclick="showModaleditKostenstelle(this)" title="Kostenstelle bearbeiten" style="color: #a94442;background-color: transparent;" type="button" class="edit btn btn-default glyphicon glyphicon-edit" value='+kostenstelle+' ></button>';
    }
}

//kostenstelle hinfufügen
function addKostenstelle(user){
    var bezeichnung = myForm.elements["bezeichnung_add"].value;
    var kostenstelle = myForm.elements["kostenstelle_add"].value;
    var kst_leiter = myForm.elements["kst_leiter_add"].value;
    var stellvertreter = myForm.elements["stellvertreter_add"].value;
    var bereichsleiter = myForm.elements["bereichsleiter_add"].value;
    
    var kst_leiter_text = $("#kst_leiter_add option:selected").text();
    var stellvertreter_text = $("#stellvertreter_add option:selected").text();
    var bereichsleiter_text = $("#bereichsleiter_add option:selected").text();
    
    if(!bezeichnung || !kostenstelle) {
        // error detected
        //e.preventDefault();
        alert("Überprüfe die Daten!");
    }
    else{
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/createkostenstelle",
            data: { kostenstelle, bezeichnung, kst_leiter, stellvertreter, bereichsleiter },
            cache: false,
            success: function(result){
                // the form has successed
                if(result){
                    alert("Kostenstelle hinzugefügt!");
                    $('#addKostenstelleModal').modal('hide');
                    t = $('#tableKostenstellen').DataTable();
                    t.row.add( [
                        kostenstelle,
                        bezeichnung,
                        kst_leiter_text,
                        stellvertreter_text,
                        bereichsleiter_text,
                        setButtons(user, kostenstelle)
                    ] ).draw( false );
                }
                else{
                    alert("Fehler beim Hinzufügen!");
                }
            }
        });
    }
    
}

// kostenstelle bearbeiten
function editKostenstelle(){
    var kostenstelle = myForm2.elements["kostenstelle_edit"].value;
    var bezeichnung = myForm2.elements["bezeichnung_edit"].value;
    var kst_leiter = myForm2.elements["kst_leiter_edit"].value;
    var stellvertreter = myForm2.elements["stellvertreter_edit"].value;
    var bereichsleiter = myForm2.elements["bereichsleiter_edit"].value;

    // Texte
    var kst_leiter_text = $("#kst_leiter_edit option:selected").text();
    var stellvertreter_text = $("#stellvertreter_edit option:selected").text();
    var bereichsleiter_text = $("#bereichsleiter_edit option:selected").text();

    if(!bezeichnung || !kostenstelle) {
       // e.preventDefault();
        alert("Überprüfe die Daten!");
    }

    $.ajax({
        type: "POST",
        url: "/editkostenstelle",
        data: { kostenstelle, bezeichnung, kst_leiter, stellvertreter, bereichsleiter },
        //cache: false,
        success: function(result){
            // the form has successed
            //alert(result);
            if(result){
                alert("Kostenstelle bearbeitet!");
                $('#editKostenstelleModal').modal('hide');
                tr_selected.find("td:eq( 0 )").html(kostenstelle);
                tr_selected.find("td:eq( 1 )").html(bezeichnung);
                tr_selected.find("td:eq( 2 )").html(kst_leiter_text);
                tr_selected.find("td:eq( 3 )").html(stellvertreter_text);
                tr_selected.find("td:eq( 4 )").html(bereichsleiter_text);
            }
            else{
                alert("Fehler beim Bearbeiten!");
            }
        }
    });
    
}

//kostenstelle löschen
function deleteKostenstelle(obj){		
    if(confirm('Kostenstelle löschen?')) {
        var table = $('#tableKostenstellen').DataTable();
        var tr_selected = $(obj).parents('tr');	
        var id = $(obj).val();
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/deletekostenstelle",
            data: { id },
            //cache: false,
            success: function(result){
                if(result){
                    alert("Kostenstelle gelöscht!");
                    // the form has successed
                    // lösche die Zeile
                    table
                            .row( tr_selected )
                            .remove()
                            .draw();
                }
                else{
                    alert("Fehler beim Löschen!");
                }
            }
        });
    }
}


