// show modal
function showModaladdBenutzer(){
    $('#name').val("");
	$('#benutzername').val("");
    $('#email').val("");
    $('#position_add').val("wahl");
    $('#rechte_add').val("wahl");
    $('#addBenutzerModal').modal({show:true});

    // benutzername dynamisch
    $("#benutzername").on( 'click', function(e){
    //name_vor = $(this).val().split(/\s+/); 
    this.value = (String($("#name").val().split(/\s+/)[1]).charAt(0) + $("#name").val().split(/\s+/)[0]).toLowerCase();  // Nuredini Ardian --> anuredini
    });
    
    // email dynamisch
    $("#email").on( 'click', function(e){
    //name_vor = $(this).val().split(/\s+/); // Nuredini Ardian --> anuredini
    this.value = (String($("#name").val().split(/\s+/)[1]).charAt(0) + $("#name").val().split(/\s+/)[0]).toLowerCase() + "@pcs.de";  // Nuredini Ardian --> anuredini@pcs.de
    });
}

function getPosition(position){
    if(position==5){
        text = "Geschäftsführer";
    }else if (position==4) {
        text = "Finanzleiter";
    }else {
        text = "Andere";
    }

    return text;
}

function getRechte(rechte){
    if(rechte==1){
        text = "Mitarbeiter";
    }else if (rechte==2) {
        text = "Buchhaltung";
    }else if (rechte==3) {
        text = "Admin Buchhaltung";
    }else {
        text = "Admin";
    }    

    return text;
}

// get values from selected row
function getEditValues(id, textOption){
    $(id).children().each( function () {
        var text = $(this).text();
        if(text == textOption){
            valOption = $(this).val();
        }
    });
    return valOption;
}

var tr_selected;

function showModaleditBenutzer(obj){
    //id_edit = $(obj).val();
    tr_selected = $(obj).parents('tr');

    // var children = $("tbody").find('button[value="'+ $(this).val() + '"]').parent().parent();

    myForm2.elements["name_edit"].value = tr_selected.find("td:eq( 0 )").text();
    myForm2.elements["benutzername_edit"].value = tr_selected.find("td:eq( 1 )").text();
    myForm2.elements["email_edit"].value = tr_selected.find("td:eq( 2 )").text();
    
    var position = tr_selected.find("td:eq( 3 )").text();
    var benutzerrechte = tr_selected.find("td:eq( 5 )").text();
    
    // set selected values
    $('#position_edit').val(getEditValues("#position_edit", position));
    $('#rechte_edit').val(getEditValues("#rechte_edit", benutzerrechte));
    
    $('#benutzer_edit').val($(obj).val());
    $('#editBenutzerModal').modal({show:true});
}

function setButtons(id_benutzer){
    return '<button onclick="deleteBenutzer(this)" title="Benutzer löschen" style="color: #a94442;background-color: transparent;" type="button" class="val btn btn-default glyphicon glyphicon-trash" value='+id_benutzer+'></button> <button onclick="showModaleditBenutzer(this)" title="Benutzer bearbeiten" style="color: #a94442;background-color: transparent;" type="button" class="edit btn btn-default glyphicon glyphicon-edit" value='+id_benutzer+'></button>';
}

//benutzer hinfufügen
function addBenutzer(user){
    // Check if there is an entered value
    if(!$('#name').val() || !$('#benutzername').val() || !$('#email').val()) {
        // Add errors highlight
        e.preventDefault();
        //$("#error_on_input").removeClass( "hide" );
        alert("Überprüfe die Daten!");
    }else {
        // create user	  
        var name = myForm.elements["name"].value;
        var benutzername = myForm.elements["benutzername"].value;
        var email = myForm.elements["email"].value;
        var position = myForm.elements["position"].value;
        var benutzerrechte = myForm.elements["rechte"].value;
        
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/addbenutzer",
            data: { name, benutzername, email, position, benutzerrechte },
            cache: false,
            success: function(result){
                // the form has successed
                //console.log(result);
                if(result!=false){
                    alert("Benutzer hinzugefügt!");
                    $('#addBenutzerModal').modal('hide');
                    t = $('#tableBenutzerverwaltung').DataTable();
                    t.row.add( [
                        name,
                        benutzername,
                        email,
                        getPosition(position),
                        '<button onclick="resetPassword(this)" type="button" class="pass_reset btn btn-secondary" value='+result+'>Zurücksetzen</button>',
                        getRechte(benutzerrechte),
                        setButtons(result)
                    ] ).draw( false );
                }else{
                    alert("Fehler beim Hinzufügen!");
                }
            }
        });
        
    }
    
}

// benutzer bearbeiten
function editBenutzer(){
    var id_edit = $("#benutzer_edit").val();
    var name = myForm2.elements["name_edit"].value;
    var benutzername = myForm2.elements["benutzername_edit"].value;
    var email = myForm2.elements["email_edit"].value;
    var position = myForm2.elements["position_edit"].value;
    var benutzerrechte = myForm2.elements["rechte_edit"].value;

    // Texte
    var position_text = $("#position_edit option:selected").text();
    var benutzerrechte_text = $("#rechte_edit option:selected").text();

    if(!name || !benutzername || !email) {
       // e.preventDefault();
        alert("Überprüfe die Daten!");
    }

    $.ajax({
        type: "POST",
        url: "/editbenutzer",
        data: { id_edit, name, benutzername, email, position, benutzerrechte },
        //cache: false,
        success: function(result){
            // the form has successed
            //alert(result);
            if(result){
                alert("Benutzer bearbeitet!");
                $('#editBenutzerModal').modal('hide');
                tr_selected.find("td:eq( 0 )").html(name);
                tr_selected.find("td:eq( 1 )").html(benutzername);
                tr_selected.find("td:eq( 2 )").html(email);
                tr_selected.find("td:eq( 3 )").html(position_text);
                tr_selected.find("td:eq( 5 )").html(benutzerrechte_text);
            }else{
                alert("Fehler beim Bearbeiten!");
            }
        }
    });
    
}

//benutzer löschen
function deleteBenutzer(obj){
    if(confirm('Benutzer löschen?')) {
        var table = $('#tableBenutzerverwaltung').DataTable();
        var tr_selected = $(obj).parents('tr');	
        var id = $(obj).val();
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/deletebenutzer",
            data: { id },
            //cache: false,
            success: function(result){
                if(result){
                    alert("Benutzer gelöscht!");
                    // the form has successed
                    // lösche die Zeile
                    table
                            .row( tr_selected )
                            .remove()
                            .draw();
                }else{
                    alert("Fehler beim Löschen!");
                }
            }
        });
    }
}

//passwort benutzer zurücksetzen
function resetPassword(obj){
    if (confirm('Passwort zurücksetzen?')) {
        var dataString = 'id='+ $(obj).val();
        // AJAX Code To Submit Form.
        $.ajax({
            type: "POST",
            url: "/resetpassword",
            data: dataString,
            //cache: false,
            success: function(result){
                // the form has successed
                if(result){
                    alert("Passwort zurückgesetzt!");
                }else{
                    alert("Fehler!");
                }
            }
        });
        
    }
}

