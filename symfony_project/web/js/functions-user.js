

function removeAlerts(){
    $("#pass_edit .alert").each( function () {
        $(this).remove();
    });
}

//änderungen speichern
function saveChanges() {
    removeAlerts();

    var passwortAlt = pass_edit.elements["passwortAlt"].value;
    var passwortNeu = pass_edit.elements["passwortNeu"].value;
    var passwortNeu2 = pass_edit.elements["passwortNeu2"].value;

    if(passwortAlt != "" && passwortNeu != "" && passwortNeu2 != ""){
                if(passwortNeu != passwortNeu2) {
                    error_msg = "Die eingegebenen Passwörter stimmten nicht überein.";
                }
                else {
                    //an ajax
                    $.ajax({
                        type: "POST",
                        url: "/editpassword",
                        data: { passwortAlt, passwortNeu, passwortNeu2 },
                        //cache: false,
                        success: function(result){
                            removeAlerts();
                            // the form has successed
                            //alert(result);
                            if(result){
                                success_msg = "Passwort erfolgreich geändert.";
                                $( "#pass_edit" ).prepend("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+success_msg+"</div>");
                            }
                            else{
                                error_msg = "Fehler.";
                                $( "#pass_edit" ).prepend("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+error_msg+"</div>");
                            }
                        }
                    });
                }
    }  
    else{
        error_msg = "Die Felder dürfen nicht leer sein.";
    }

    if(typeof error_msg !== 'undefined'){
        $( "#pass_edit" ).prepend("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+error_msg+"</div>");
    }

}