<?php

namespace AppBundle\Service;

use AppBundle\Entity\BestInvestantrag;
use AppBundle\Entity\Investantraege;
use AppBundle\Entity\Benutzerverwaltung;
use AppBundle\Entity\Kostenstellenplan;

use Doctrine\ORM\EntityManager;

class ManageInvantraege
{
    private $em;
    private $bestaetigungen;
    private $investantraege;
    private $kostenstellenplaene;
    private $benutzerverwaltungen;
    private $user;
    private $manageBest;

    // constructor
    public function __construct(EntityManager $em, ManageBest $m)  {
        //$this->bestaetiger = $bestaetiger;
        $this->manageBest = $m;
        $this->em = $em;
        $this->bestaetigungen = $this->em->getRepository('AppBundle:BestInvestantrag');
        $this->investantraege = $this->em->getRepository('AppBundle:Investantraege');
        $this->kostenstellenplaene = $this->em->getRepository('AppBundle:Kostenstellenplan');
        $this->benutzerverwaltungen = $this->em->getRepository('AppBundle:Benutzerverwaltung');
    }

    public function getManageBest(){
        return $this->manageBest;
    }

    /**
     * Set user
     *
     * @param int $user
     *
     * @return ManageBest
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\Benutzerverwaltung
     */
    public function getUser()
    {
        return $this->em->getRepository(Benutzerverwaltung::class)->find($this->user);
    }

    public function getBenutzerKst(Kostenstellenplan $k)
    {
        $benutzer = $this->benutzerverwaltungen->findById(array($k->getKstLeiter()->getId(), $k->getStellvertreter()->getId(), $k->getBereichsleiter()->getId()));
        return $benutzer;
    }    

    public function getInvName()
    {
        $investantraege = $this->investantraege->findAll();
 
        $y = date("y");
        $m = date("m");
        $d = date("d");

        if(!empty($investantraege)){
            $max = 0;
            $id_name = "";
            foreach($investantraege as $i) {
                if($i->getId()>$max){
                    $max = $i->getId();
                    $id_name = $i->getIdName();
                }
            }

            $jahr = substr($id_name, 2, 2);
            $id_inv = substr($id_name, 4, 3);
                        
            /*
            $d = "18";
            $m = "01";
            $y = "19";
            */
            
            if($m>=07 && $m<=12 && $jahr<$y+1){
                // geschäftsjahr
                $id_name = "IV".($y+1)."001";
            }
            else{
                $id_name = "IV".($jahr).sprintf("%03d",$id_inv+1);
            }
            
        }
        else{
            if($m>=07 && $m<=12){
                $id_name = "IV".($y+1)."001";
            }
            else {
                $id_name = "IV".($y)."001";
            }
        }

        return $id_name;

    }

    public function addInvestantrag($data, Benutzerverwaltung $antragsteller, Kostenstellenplan $kostenstelle, $wunschtermin, $budget, $gesamt, $begruendung){
        $time = strtotime($wunschtermin);
        $datum = date('Y-m-d',$time);

        $sql = "INSERT INTO investantraege (id_name, antragsteller, kostenstelle, wunschtermin, budget, gesamt, begruendung) 
                VALUES (:id_name, :antragsteller, :kostenstelle, :wunschtermin, :budget, :gesamt, :begruendung)";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('id_name', $this->getInvName());
        $stmt->bindValue('antragsteller', $antragsteller->getId());
        $stmt->bindValue('kostenstelle', $kostenstelle->getId());
        $stmt->bindValue('wunschtermin', $datum);
        $stmt->bindValue('budget', $budget);
        $stmt->bindValue('gesamt', $gesamt);
        $stmt->bindValue('begruendung', $begruendung);
        
        try {
            $result = $stmt->execute();
        } catch (\Exception $e) {
            $result = false;
        }

        if($result){
            $idAntrag = $this->em->getConnection()->lastInsertId();

            $investantrag = $this->em->getRepository(Investantraege::class)->find($idAntrag);
            if($this->manageBest->addBestInv($investantrag, $antragsteller, $kostenstelle)){
                if($this->addDatenInv($investantrag, $antragsteller, $data)){
                    $result = true;
                    $betreff = "Investantrag zu bestätigen";
                    $nachricht = "Ein neuer Antrag #". $investantrag->getIdName() ." wurde gestellt und wartet auf Deine Bestätigung.<br><a href='intranet/repo-investimenti/pages/investitionsantraege.php?id=".$investantrag->getIdName()."'>Zum Antrag</a>";
                    $header = 'Content-type: text/html; charset=UTF-8' . "\r\n";
                    $header .= 'From: PCS Investitionsplan-System <investsystem@pcs.de>' . "\r\n";
                    $this->sendEmail($this->manageBest->getEmailsBenutzer(), $betreff, $nachricht, $header);
                }
                else {
                    $result = false;
                }
            }
            else{
                $result = false;
            }
	    }

        if($result){
            $result = $investantrag;
        }

        return $result;
    }

    public function addDatenInv(Investantraege $investantrag, Benutzerverwaltung $b, $data){
        for($i = 0; $i <= count($data); $i++){
		    if ($i>0 && ($i % 4 == 0)) {
                $bezeichnung=$data[$i-4];
                $menge=$data[$i-3];
                $ep=$data[$i-2];
                $summe=$data[$i-1];
                
                $sql = "INSERT INTO daten_investantrag(investantrag, bezeichnung, menge, ep, summe)
                        VALUES (:investantrag, :bezeichnung, :menge, :ep, :summe)";
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->bindValue('investantrag', $investantrag->getId());
                $stmt->bindValue('bezeichnung', $bezeichnung);
                $stmt->bindValue('menge', $menge);
                $stmt->bindValue('ep', $ep);
                $stmt->bindValue('summe', $summe);

                try {
                    $result = $stmt->execute();
                } catch (\Exception $e) {
                    $result = false;
                }
            }
		}
        return $result;
        
    }

    public function sendEmail($benutzer, $betreff, $nachricht, $header){
        if (!empty($benutzer)) {
            foreach($benutzer as $b){
                $empfaenger = $b->getEmail();
                //mail($empfaenger, $betreff, $nachricht, $header);
            }
        }
    }

    public function deleteInvestantrag($investantrag){
        if($this->manageBest->deleteBestinvestantrag($investantrag)){
            if($this->manageBest->deleteDateninvestantrag($investantrag)){
                $sql = "DELETE FROM investantraege WHERE id = :id";
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->bindValue('id', $investantrag);
                try {
                    $result = $stmt->execute();
                } catch (\Exception $e) {
                    $result = false;
                }
            }
            else {
                $result = false;
            }
        }
        else {
            $result = false;
        }
        return $result;

    }

}