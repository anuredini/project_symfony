<?php

namespace AppBundle\Service;

use AppBundle\Entity\BestInvestantrag;
use AppBundle\Entity\Investantraege;
use AppBundle\Entity\Benutzerverwaltung;
use AppBundle\Entity\Kostenstellenplan;

use Doctrine\ORM\EntityManager;

class ManageBenutzer
{
    private $benutzer;
    private $manageBest;
    private $em;

    // constructor
    public function __construct(EntityManager $em)  {
        $this->em = $em;
        /*
        $this->bestaetigungen = $this->em->getRepository('AppBundle:BestInvestantrag');
        $this->investantraege = $this->em->getRepository('AppBundle:Investantraege');
        $this->kostenstellenplaene = $this->em->getRepository('AppBundle:Kostenstellenplan');
        $this->benutzerverwaltungen = $this->em->getRepository('AppBundle:Benutzerverwaltung');
        */
    }

    public function getPosition(Benutzerverwaltung $b){
        if($b->getPosition()==5){
            $position = "Geschäftsführer";
        }elseif ($b->getPosition()==4) {
            $position = "Finanzleiter";
        }else {
            $position = "Andere";
        }

        return $position;
    }

    public function getRechte(Benutzerverwaltung $b){
        if($b->getBenutzerrechte()==1){
            $rechte = "Mitarbeiter";
        }elseif ($b->getBenutzerrechte()==2) {
            $rechte = "Buchhaltung";
        }elseif ($b->getBenutzerrechte()==3) {
            $rechte = "Admin Buchhaltung";
        }else {
            $rechte = "Admin";
        }    

        return $rechte;
    }


}