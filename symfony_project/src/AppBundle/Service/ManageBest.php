<?php

namespace AppBundle\Service;

use AppBundle\Entity\BestInvestantrag;
use AppBundle\Entity\Investantraege;
use AppBundle\Entity\Benutzerverwaltung;
use AppBundle\Entity\Kostenstellenplan;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;

class ManageBest
{
    private $em;
    private $bestaetigungen;
    private $investantraege;
    private $kostenstellenplaene;
    private $benutzerverwaltungen;
    private $user;
    private $position;
    /**
     * @ORM\OneToMany(targetEntity="Benutzerverwaltung", mappedBy="id")
     */
    private $emailsBenutzer;

    //constructor
    public function __construct(EntityManager $em)  {
        //$this->emailsBenutzer = new ArrayCollection();
        $this->em = $em;
        $this->bestaetigungen = $this->em->getRepository('AppBundle:BestInvestantrag');
        $this->investantraege = $this->em->getRepository('AppBundle:Investantraege');
        $this->kostenstellenplaene = $this->em->getRepository('AppBundle:Kostenstellenplan');
        $this->benutzerverwaltungen = $this->em->getRepository('AppBundle:Benutzerverwaltung');
    }

    public function getMaxPosition($id)
    {
        $bestaetigungen = $this->bestaetigungen->findByInvestantrag($id);
        $k = $this->investantraege->find($id);
        $k = $k->getKostenstelle();

        $max = 0;
        $found_item = null;

        foreach($bestaetigungen as $b) {
            /*
            if($b->getPosition() > $max)
            {
                $max = $b->getPosition();
                $found_item = $b->getId();
            }
            */
            if($this->getPositionUser($b->getBestaetiger(), $k) > $max)
            {
                //$max = $b->getPosition();
                //$found_item = $b->getId();
                $max = $this->getPositionUser($b->getBestaetiger(), $k);
            }
        }

        return $max;
    }

    public function getPositionUser(Benutzerverwaltung $b, Kostenstellenplan $k)
    {
        if($b->getPosition() == 5){
            $position = 5;
        }else if($b->getPosition() == 4){
            $position = 4;
        }else{
            if($b->getId() == $k->getBereichsleiter()->getId()){
                $position = 3;
            }else if($b->getId() == $k->getKstLeiter()->getId() || $b->getId() == $k->getStellvertreter()->getId()){
                $position = 2;
            }else{
                $position = 1;
            }
        }

        return $position;
    }    

    /**
     * Set user
     *
     * @param int $user
     *
     * @return ManageBest
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\Benutzerverwaltung
     */
    public function getUser()
    {
        return $this->em->getRepository(Benutzerverwaltung::class)->find($this->user);
    }

    public function countLaufendeAntraege()
    {
        $investantraege = $this->investantraege->findAll();
        $count = 0;

        foreach($investantraege as $i) {
            if($i->getAntragsteller()->getId() == $this->user && ($this->getMaxPosition($i->getId())) < 5){
                $count++;
            }
            /*
            if(($benutzer_id == $row["kst_leiter"] || $benutzer_id == $row["stellvertreter"])){
                if($row["MaxPosition"] == 1){
                    $status = "Zu bestätigen";
                }
            }
            */
        }
        return $count;
    }

    public function getStatusInvantrag(Investantraege $investantrag)
    {    
        //$investantraege = $this->investantraege->findById($invantrag);

        foreach($investantrag as $i) {
            if($this->getMaxPosition($i->getId()) == 5){
                $status = "Erledigt";
            }
            elseif ($this->getMaxPosition($i->getId()) == 4) {
                $status = "Warte auf Geschäftsführer";
            }
            elseif ($this->getMaxPosition($i->getId()) == 3) {
                $status = "Warte auf Finanzleiter";
            }
            elseif ($this->getMaxPosition($i->getId()) == 2) {
                $status = "Warte auf Bereichsleiter";
            }
            else {
                $status = "Warte auf Kostenstellenleiter/Stellvetreter";
            }
        }
        return $status;
    }

    public function addBestInv(Investantraege $investantrag, Benutzerverwaltung $bestaetiger, Kostenstellenplan $k){
        $this->setPositionEBenutzer($bestaetiger, $k);
        $datum = date("y.m.d");
        if($this->getMaxPosition($investantrag->getId()) < $this->getPosition()){
            $sql = "INSERT INTO best_investantrag(investantrag, bestaetiger, datum, position) 
                    VALUES (:investantrag, :bestaetiger, :datum, :position)";
            $stmt = $this->em->getConnection()->prepare($sql);
            $stmt->bindValue('investantrag', $investantrag->getId());
            $stmt->bindValue('bestaetiger', $bestaetiger->getId());
            $stmt->bindValue('datum', $datum);
            $stmt->bindValue('position', $this->getPosition());

            try {
                $result = $stmt->execute();
            } catch (\Exception $e) {
                $result = false;
            }
        }else {
            $result = false;
        }

        return $result;
        
    }

    public function setPositionEBenutzer(Benutzerverwaltung $b, Kostenstellenplan $k)
    {
        if($b->getPosition() == 5){
            $position = 5;
        }else if($b->getPosition() == 4){
            $position = 4;
            $benutzer = $this->benutzerverwaltungen->findByPosition($position+1);
        }else{
            if($b->getId() == $k->getBereichsleiter()->getId()){
                $position = 3;
                $benutzer = $this->benutzerverwaltungen->findByPosition($position+1);
            }else if($b->getId() == $k->getKstLeiter()->getId() || $b->getId() == $k->getStellvertreter()->getId()){
                $position = 2;
                $benutzer = $this->benutzerverwaltungen->findById($b->getId());
            }else{
                $position = 1;
                $benutzer = $this->benutzerverwaltungen->findById(array($k->getKstLeiter()->getId(), $k->getStellvertreter()->getId()));
            }
        }

        if(isset($benutzer)) {
            $this->emailsBenutzer = array_filter($benutzer);
        }
        $this->position = $position;
        //return $position;

    }

    public function getEmailsBenutzer(){
        return $this->emailsBenutzer;
    }

    public function getPosition(){
        return $this->position;
    }   

    public function getStatus(Investantraege $investantrag)
    {

        $user = $this->user;
        //$investantrag = $this->investantraege->findById($invantrag);
        $benutzer = $this->benutzerverwaltungen->findById($user);
        $status = null;
        
        //Fall Antragsteller
        if($user == $investantrag->getAntragsteller()->getId()){
                if($this->getMaxPosition($investantrag->getId()) == 5){
                    // CEO confermato
                    $status = "Erledigt";
                }
                else if($this->getMaxPosition($investantrag->getId()) == 4){
                    // Capo finanze confermato
                    $status = "Warte auf Geschäftsführer";
                }
                else if($this->getMaxPosition($investantrag->getId()) == 3){
                    $status = "Warte auf Finanzleiter";
                }
                else if($this->getMaxPosition($investantrag->getId()) == 2){
                    $status = "Warte auf Bereichsleiter";
                }
                else if($this->getMaxPosition($investantrag->getId()) == 1){
                    $status = "Warte auf Kostenstellenleiter/Stellvetreter";
                }
        }

        else{
            //Fall kst_leiter oder stellvertreter
            if($user == $investantrag->getKostenstelle()->getKstLeiter()->getId() || $user == $investantrag->getKostenstelle()->getStellvertreter()->getId()){
                if($this->getMaxPosition($investantrag->getId()) == 1){
                    $status = "Zu bestätigen";
                }
                /*
                else if($this->getMaxPosition($invantrag) > 1){
                    $status = "vom nächsten bestätigt";
                }
                */
                else{
                    $status = "Bestätigt";
                }
            }
            
            //Fall bereichsleiter
            else if($user == $investantrag->getKostenstelle()->getBereichsleiter()->getId()){
                if($this->getMaxPosition($investantrag->getId()) < 2){
                    $status = "Warte auf Vorherigen";
                }else if($this->getMaxPosition($investantrag->getId()) == 2){
                    $status = "Zu bestätigen";
                }
                else{
                    $status = "Bestätigt";
                }
            }
            
            /*
            else if($benutzer[0]->getPosition() == 1){
                if($this->getMaxPosition($invantrag) == 5){
                    $status = "vom Geschäftsführer bestätigt";
                }
                else if($this->getMaxPosition($invantrag) == 4){
                    $status = "vom Finanzleiter bestätigt";
                }
                else if($this->getMaxPosition($invantrag) == 3){
                    $status = "vom Bereichsleiter bestätigt";
                }
                else if($this->getMaxPosition($invantrag) == 2){
                    $status = "Vom Kostenstellenleiter/Stellvetreter bestätigt";
                }
                else{
                    $status = "Warte auf alle Bestätigungen";
                }
            }
            */
            
            //Fall finanzleiter
            else if($benutzer[0]->getPosition() == 4){
                if($this->getMaxPosition($investantrag->getId()) < 3){
                    $status = "Warte auf Vorherigen";
                }else if($this->getMaxPosition($investantrag->getId()) == 3){
                    $status = "Zu bestätigen";
                }
                else if($this->getMaxPosition($investantrag->getId()) == 4){
                    $status = "Bestätigt";
                }
                else{
                    $status = "Vom Geschäftsführer bearbeitet";
                }
            }
            
            //Fall Geschäftsführer
            else if($benutzer[0]->getPosition() == 5){
                if($this->getMaxPosition($investantrag->getId()) < 4){
                    $status = "Warte auf Vorherigen";
                }else if($this->getMaxPosition($investantrag->getId()) == 4){
                    $status = "Zu bestätigen";
                }
                else{
                    $status = "Bestätigt";
                }
            }
        
        }
        
        return $status;
    }

    public function getStatusNumber(Investantraege $investantrag)
    {

        $user = $this->user;
        //$investantrag = $this->investantraege->findById($invantrag);
        $benutzer = $this->benutzerverwaltungen->findById($user);
        $status = null;
        
        //Fall Antragsteller
        if($user == $investantrag->getAntragsteller()->getId()){
                if($this->getMaxPosition($investantrag->getId()) == 5){
                    // CEO confermato
                    //$status = "Erledigt";
                    $status = 3;
                }
                else{
                    // Capo finanze confermato
                    //$status = "Laufend";
                    $status = 1;
                }
        }

        else{
            //Fall kst_leiter oder stellvertreter
            if($user == $investantrag->getKostenstelle()->getKstLeiter()->getId() || $user == $investantrag->getKostenstelle()->getStellvertreter()->getId()){
                if($this->getMaxPosition($investantrag->getId()) == 1){
                    //$status = "Zu bestätigen";
                    $status = 2;
                }
                /*
                else if($this->getMaxPosition($invantrag) > 1){
                    $status = "vom nächsten bestätigt";
                }
                */
                else if($this->getMaxPosition($investantrag->getId()) == 2){
                    //$status = "Bestätigt";
                    $status = 4;
                }
            }
            
            //Fall bereichsleiter
            else if($user == $investantrag->getKostenstelle()->getBereichsleiter()->getId()){
                if($this->getMaxPosition($investantrag->getId()) == 2){
                    //$status = "Zu bestätigen";
                    $status = 2;
                }
                else if($this->getMaxPosition($investantrag->getId()) == 3){
                    //$status = "Bestätigt";
                    $status = 4;
                }
            }
            
            /*
            else if($benutzer[0]->getPosition() == 1){
                if($this->getMaxPosition($invantrag) == 5){
                    $status = "vom Geschäftsführer bestätigt";
                }
                else if($this->getMaxPosition($invantrag) == 4){
                    $status = "vom Finanzleiter bestätigt";
                }
                else if($this->getMaxPosition($invantrag) == 3){
                    $status = "vom Bereichsleiter bestätigt";
                }
                else if($this->getMaxPosition($invantrag) == 2){
                    $status = "Vom Kostenstellenleiter/Stellvetreter bestätigt";
                }
                else{
                    $status = "Warte auf alle Bestätigungen";
                }
            }
            */
            
            //Fall finanzleiter
            else if($benutzer[0]->getPosition() == 4){
                if($this->getMaxPosition($investantrag->getId()) == 3){
                    //$status = "Zu bestätigen";
                    $status = 2;
                }
                else if($this->getMaxPosition($investantrag->getId()) == 4){
                    //$status = "Bestätigt";
                    $status = 4;
                }
            }
            
            //Fall Geschäftsführer
            else if($benutzer[0]->getPosition() == 5){
                if($this->getMaxPosition($investantrag->getId()) == 4){
                    //$status = "Zu bestätigen";
                    $status = 2;
                }
                else if($this->getMaxPosition($investantrag->getId()) == 5){
                    //$status = "Bestätigt";
                    $status = 4;
                }
            }
        
        }
        
        return $status;
    }

    public function isMyKostenstelle(Benutzerverwaltung $b, Kostenstellenplan $k){
        $kostenstellenplaene = $this->kostenstellenplaene->findAll();
        $response = false;

        if($k->getKstLeiter()->getId() == $b->getId() || $k->getStellvertreter()->getId() == $b->getId() || $k->getBereichsleiter()->getId() == $b->getId()){
            $response = true;
        }

        return $response;
    }

    public function getVolumenKostenstellen()
    {
        $investantraege = $this->investantraege->findAll();
        $kostenstellenplaene = $this->kostenstellenplaene->findAll();
        $gesamt = 0;

        foreach($kostenstellenplaene as $k) {
            foreach($investantraege as $i) {
                if($k->getId() == $i->getKostenstelle()->getId()){
                    $gesamt += $i->getGesamt();
                }
            }
            if($gesamt != 0){
                $termin[] = array('id' => $k->getId(), 
                    'bezeichnung'   => $k->getBezeichnung(), 
                    'name'   => $k->getBereichsleiter()->getName(),
                    'summe'  => number_format((float)($gesamt), 2, '.', ''));
                $gesamt = 0;
            }
        }
        return $termin;
    }    

    public function countZuBestAntraege()
    {
        $investantraege = $this->investantraege->findAll();
        $count = 0;

        foreach($investantraege as $i) {
            if($this->getStatus($i) == "Zu bestätigen"){
                $count++;
            }
        }
        return $count;
    }

    public function countErledigteAntraege()
    {
        $investantraege = $this->investantraege->findAll();
        $count = 0;

        foreach($investantraege as $i) {
            if($i->getAntragsteller()->getId() == $this->user && ($this->getMaxPosition ($i->getId())) == 5){
                $count++;
            }
        }
        return $count;
    }

    public function countBestAntraege()
    {
        $investantraege = $this->investantraege->findAll();
        $count = 0;
        $id="";

        foreach($investantraege as $i) {
            if(($i->getAntragsteller()->getId()) != $this->user){
                foreach($this->bestaetigungen->findByInvestantrag($i->getId()) as $b) {
                    if($b->getBestaetiger()->getId() == $this->user){
                        $count++;
                        $id.="/".$i->getId();
                    }
                }
            }
        }
        return $id;
    }

    public function deleteBestinvestantrag($investantrag){
        $sql = "DELETE FROM best_investantrag WHERE investantrag = :id";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('id', $investantrag);
        
        try {
            $result = $stmt->execute();
        } catch (\Exception $e) {
            $result = false;
        }
        return $result;

    }

    public function deleteDateninvestantrag($investantrag){
        $sql = "DELETE FROM daten_investantrag WHERE investantrag = :id";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('id', $investantrag);
        
        try {
            $result = $stmt->execute();
        } catch (\Exception $e) {
            $result = false;
        }
        return $result;

    }

    public function getBestFromPosition(Investantraege $i, $position){
        $bestaetigungen = $this->bestaetigungen->findByInvestantrag($i);

        foreach($bestaetigungen as $b) {
            if($b->getPosition() == $position){
                return $b;
                break;
            }
        }
    }   

}