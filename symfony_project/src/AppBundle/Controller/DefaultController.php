<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\ManageBenutzer;
use AppBundle\Service\ManageBest;
use AppBundle\Service\ManageInvantraege;
use AppBundle\Entity\Kostenstellenplan;
use AppBundle\Entity\Benutzerverwaltung;
use AppBundle\Entity\Investantraege;
use AppBundle\Entity\DatenInvestantrag;
use AppBundle\Entity\BestInvestantrag;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class DefaultController extends Controller
{

    /**
     * @Route("/test")
     */
    public function test(UserPasswordEncoderInterface $encoder)
    {
        $user = new Benutzerverwaltung();
        $pass = $encoder->encodePassword($user,"pcspcs");
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $usr=$usr->getName();
        //return new Response('<html><body>'.$usr.'</body></html>');

        $token = $this->get('security.token_storage')->getToken();
        return new Response('<html><body>'.$token.'</body></html>');
    }

    /**
     * @Route("/admin")
     */
    public function admin()
    {
        return new Response('<html><body>Admin page!</body></html>');
    }

    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request, AuthenticationUtils $a)
    {

        $errors = $a->getLastAuthenticationError();
        return $this->render('login.html.twig', [
            //'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'errors' => $errors,
        ]);

        /*
        try {
            $authenticatedToken = $a->authenticate($unauthenticatedToken);
        } catch (AuthenticationException $failed) {
            // authentication failed
            return new Response('<html><body>Fehler!</body></html>');
        }
        */

    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboardAction(Request $request, ManageBest $manageBest)
    {
        /*
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('
                SELECT b.id FROM AppBundle:Benutzerverwaltung b
                WHERE b.position > :position 
            ')
            ->setParameter('position', '1');

        $benutzer = $query->getResult();
        */

        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $manageBest->setUser($usr->getId());

        return $this->render('dashboard.html.twig', array(
            'manageBest' => $manageBest
        ));

    }

    /**
     * @Route("/profil", name="profil")
     */
    public function benutzerAction(Request $request)
    {

        $benutzer = $this->get('security.token_storage')->getToken()->getUser();

        return $this->render('benutzer.html.twig', array(
            'benutzer' => $benutzer
        ));

    }  

    /**
     * @Route("/view", name="view")
     */
    public function viewAction(Request $request, ManageBest $manageBest, ManageInvantraege $manageInvantrag)
    {

        $em = $this->getDoctrine()->getManager();

        /*
        $benutzerverwaltung = $this->getDoctrine()->getRepository('AppBundle:Benutzerverwaltung')->findAll();
        $investantraege = $this->getDoctrine()->getRepository('AppBundle:Investantraege')->findAll();
        $bestaetigungen = $this->getDoctrine()->getRepository('AppBundle:BestInvestantrag')->findAll(134);
        */

        /*
        foreach($bestaetigungen as $b) {
            //echo($b->getInvestantrag()->getIdName()."<br>");
            echo($b->getId()."<br>");
            echo($b->getPosition()."<br>");
            //dump($b);
        }    
        echo "<br><br>";
        */

        /*
        $manageBest = $manageBest->setUser(4);

        $em = $this->getDoctrine()->getManager();
        //$all = $em->getRepository(Benutzerverwaltung::class)->findAll();
        $investantraege = $this->getDoctrine()->getRepository('AppBundle:Investantraege')->findAll();

        echo  $manageBest->getUser()->getName();
        */

        /*
        $b = $em->getRepository(Benutzerverwaltung::class)->find(5);
        $k = $em->getRepository(Kostenstellenplan::class)->find(10);
        $manageInvantrag->setStatPos($b, $k);
        echo "staus: ".$manageInvantrag->getStatus()."<br>";
        echo "position: ".$manageInvantrag->getPosition();
        */

        /*
        foreach($manageInvantrag->setInvname() as $i) {
            //echo($v["id"]."<br>");
        }
        */
        
        //$manageInvantrag->setInvName();
        //echo($manageInvantrag->getInvName());

        $a = array(4,5);
        $benutzerverwaltung = $em->getRepository('AppBundle:Benutzerverwaltung')->findById($a);
        //$benutzerverwaltung2 = $em->getRepository('AppBundle:Benutzerverwaltung')->findById(5);
        //$b3 = $benutzerverwaltung->append($benutzerverwaltung2);
        foreach ($benutzerverwaltung as $b) {
            //echo ($b->getId()."<br>");
        }
        //var_dump($benutzerverwaltung);

        $id_investantrag = 194;
        $em = $this->getDoctrine()->getManager();

        $k = $em->getRepository(Kostenstellenplan::class)->find(500);
        //$manageBest->setUser(128);

        $benutzer = $manageInvantrag->getBenutzerKst($k);
        foreach($benutzer as $d){
            echo $d->getName()."<br>";
        }
        
        die();

        /*
        $session = $request->getSession();
        die($session->get('name'));
        */

    }

    /**
     * @Route("/kostenstellenuebersicht", name="kostenstellenuebersicht")
     */
    public function kostenstellenuebersichtAction(Request $request)
    {
        $benutzerverwaltung = $this->getDoctrine()->getRepository('AppBundle:Benutzerverwaltung')->findAll();
        $kostenstellen = $this->getDoctrine()->getRepository('AppBundle:Kostenstellenplan')->findAll();
        
        return $this->render('kostenstellenuebersicht.html.twig', array(
            'userRechte' => true,
            'benutzer' => $benutzerverwaltung,
            'kostenstellen' => $kostenstellen
        ));
    }

    /**
     * @Route("/benutzerverwaltung", name="benutzerverwaltung")
     */
    public function benutzerverwaltungAction(Request $request, ManageBenutzer $manageBenutzer)
    {
        $benutzerverwaltung = $this->getDoctrine()->getRepository('AppBundle:Benutzerverwaltung')->findAll();
        $kostenstellen = $this->getDoctrine()->getRepository('AppBundle:Kostenstellenplan')->findAll();
        
        return $this->render('benutzerverwaltung.html.twig', array(
            'userRechte' => true,
            'benutzer' => $benutzerverwaltung,
            'manageBenutzer' => $manageBenutzer,
            'kostenstellen' => $kostenstellen
        ));
    }

    /**
     * @Route("/investitionsantraege/{param}", name="investitionsantraege")
     */
    public function investitionsantraegeAction($param=0,Request $request, ManageBest $manageBest)
    {
        $benutzerverwaltung = $this->getDoctrine()->getRepository('AppBundle:Benutzerverwaltung')->findAll();
        $kostenstellen = $this->getDoctrine()->getRepository('AppBundle:Kostenstellenplan')->findAll();
        $investantraege = $this->getDoctrine()->getRepository('AppBundle:Investantraege')->findAll();

        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $manageBest->setUser($usr->getId());

        return $this->render('investitionsantraege.html.twig', array(
            'userRechte' => true,
            'benutzer' => $benutzerverwaltung,
            'kostenstellen' => $kostenstellen,
            'manageBest' => $manageBest,
            'param' => $param,
            'investantraege' => $investantraege
        ));
    }

    /**
     * @Route("/investantrag/{id}", name="investantrag")
     */
    public function investantragAction(Request $request, $id, ManageBest $b, ManageBest $manageBest)
    {
        $investantrag = $this->getDoctrine()->getRepository(Investantraege::class)->find($id);
        $bestaetigungen = $this->getDoctrine()->getRepository(BestInvestantrag::class)->findByInvestantrag($id);
        $dateninvestantrag = $this->getDoctrine()->getRepository(DatenInvestantrag::class)->findByInvestantrag($id);

        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $manageBest->setUser($usr->getId());

        if($manageBest->getStatus($investantrag)!=null){
            //antrag ansehen erlaubt
            //get data and send to ajax
            return $this->render(
                'investantrag.html.twig', array(
                'investantrag' => $investantrag,
                'bestaetigungen' => $bestaetigungen,
                'managebest' => $b,
                'dateninvestantrag' => $dateninvestantrag
            ));
        }else {
           //return new Response('<html><body>Nicht erlaubt</body></html>');
           return $this->redirectToRoute('investitionsantraege');
        }

    }

    /**
     * @Route("/createkostenstelle", name="createkostenstelle")
     */
    public function createKostenstelleAction(Request $request)
    {

        $id=$_POST['kostenstelle'];
        $bezeichnung=$_POST['bezeichnung'];
        $kst_leiter=$_POST['kst_leiter'];
        $stellvertreter=$_POST['stellvertreter'];
        $bereichsleiter=$_POST['bereichsleiter'];
        
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to your action: createAction(EntityManagerInterface $em)

        $em = $this->getDoctrine()->getManager();
        /*
        $kst_leiter = $em->getRepository(Benutzerverwaltung::class)->find($kst_leiter);
        $stellvertreter = $em->getRepository(Benutzerverwaltung::class)->find($stellvertreter);
        $bereichsleiter = $em->getRepository(Benutzerverwaltung::class)->find($bereichsleiter);

        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($kostenstelle);

        // actually executes the queries (i.e. the INSERT query)
        //$em->flush();
        */

        // with sql query
        $sql = "INSERT INTO kostenstellenplan (id, bezeichnung, kst_leiter, stellvertreter, bereichsleiter) 
                VALUES (:id, :bezeichnung, :kst_leiter, :stellvertreter, :bereichsleiter)";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('id', $id);
        $stmt->bindValue('bezeichnung', $bezeichnung);
        $stmt->bindValue('kst_leiter', $kst_leiter);
        $stmt->bindValue('stellvertreter', $stellvertreter);
        $stmt->bindValue('bereichsleiter', $bereichsleiter);
        //$result = $stmt->execute();
        
        try {
            $result = $stmt->execute();
        } catch (\Exception $e) {
            $result = false;
        }

        die ($result);

        /*
        foreach($kostenstelle as $k) {
            echo($k->getId()->getName()."<br>");
            echo($k->getBezeichnung()."<br>");
            echo($k->setKstLeiter()->getName()."<br>");
            echo($k->setKstLeiter()->getName()."<br>");
            echo($k->getBereichsleiter()->getName()."<br>");
        }
        */

    }    

    /**
     * @Route("/editkostenstelle", name="editkostenstelle")
     */
    public function editKostenstelleAction(Request $request)
    {

        $id=$_POST['kostenstelle'];
        $bezeichnung=$_POST['bezeichnung'];
        $kst_leiter=$_POST['kst_leiter'];
        $stellvertreter=$_POST['stellvertreter'];
        $bereichsleiter=$_POST['bereichsleiter'];

        $em = $this->getDoctrine()->getManager();

        $sql = "UPDATE kostenstellenplan SET bezeichnung = :bezeichnung, kst_leiter = :kst_leiter, stellvertreter = :stellvertreter, bereichsleiter = :bereichsleiter 
                WHERE kostenstellenplan.id = :id";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('id', $id);
        $stmt->bindValue('bezeichnung', $bezeichnung);
        $stmt->bindValue('kst_leiter', $kst_leiter);
        $stmt->bindValue('stellvertreter', $stellvertreter);
        $stmt->bindValue('bereichsleiter', $bereichsleiter);
        
        try {
            $result = $stmt->execute();
        } catch (\Exception $e) {
            $result = false;
        }

        die ($result);

    }

    /**
     * @Route("/deletekostenstelle", name="deletekostenstelle")
     */
    public function deleteKostenstelleAction(Request $request)
    {

        $id=$_POST['id'];

        $em = $this->getDoctrine()->getManager();

        $sql = "DELETE FROM kostenstellenplan WHERE id = :id";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('id', $id);
        
        try {
            $result = $stmt->execute();
        } catch (\Exception $e) {
            $result = false;
        }

        die ($result);

    }

    /**
     * @Route("/addinvestantrag", name="addinvestantrag")
     */
    public function addInvantragAction(Request $request, ManageInvantraege $manageInv, ManageBest $manageBest)
    {

        $data = json_decode($_POST['info_array'], true);
        $benutzer_id = $_POST['benutzer_id'];
        $benutzer_position = $_POST['benutzer_position'];
        $kostenstelle = $_POST['kostenstelle'];
        $datum = $_POST['datum'];
        $budget = $_POST['budget'];
        $gesamtsumme = $_POST['gesamtsumme'];
        $begruendung = $_POST['begruendung'];
        //$naechste_benutzer = array();
        $naechste_benutzer = "";

        $benutzer_id = 4;

        $em = $this->getDoctrine()->getManager();

        $benutzer = $em->getRepository(Benutzerverwaltung::class)->find($benutzer_id);
        $kst = $em->getRepository(Kostenstellenplan::class)->find($kostenstelle);
        $manageBest->setUser($benutzer_id);

        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to your action: createAction(EntityManagerInterface $em)

        $result = $manageInv->addInvestantrag($data, $benutzer, $kst, $datum, $budget, $gesamtsumme, $begruendung);

        
        foreach ($manageInv->getManageBest()->getEmailsBenutzer() as $benutzer) {
            $naechste_benutzer.=$benutzer->getEmail().", ";
        }

        if($result!=false){
            $noerror = true;
            die (json_encode(array("id_invantrag" => $result->getId(), "id_name" => $result->getIdName(), "status" => $manageBest->getStatus($result), "noerror" => $noerror, "naechste_benutzer" => $naechste_benutzer)));
        }
        else{
            $noerror = false;
            die (json_encode(array("noerror" => $noerror)));
        }


    }

    /**
     * @Route("/deleteinvestantrag", name="deleteinvestantrag")
     */
    public function deleteInvestantragAction(Request $request, ManageInvantraege $m)
    {

        $userRechte = false;
        $investantrag = $_POST['id'];

        $em = $this->getDoctrine()->getManager();

        if($userRechte){
            $result = $m->deleteInvestantrag($investantrag);
        }
        else{
            $result = false;
        }

        die ($result);

    }

    /**
     * @Route("/getdateninvestantrag", name="getdateninvestantrag")
     */
    public function getDatenInvestantragAction(Request $request, ManageInvantraege $m, ManageBest $manageBest)
    {
        $id_investantrag = $_POST['id'];
        $em = $this->getDoctrine()->getManager();
        $daten = array();
        $bestaetigungen = array();

        $investantrag = $em->getRepository(Investantraege::class)->find($id_investantrag);
        $manageBest->setUser(128);

        if($manageBest->getStatus($investantrag)!=null){
            //antrag ansehen erlaubt
            //get data and send to ajax
            $daten_investantrag = $em->getRepository('AppBundle:DatenInvestantrag')->findByInvestantrag($investantrag->getId());
            $best_investantrag = $em->getRepository('AppBundle:BestInvestantrag')->findByInvestantrag($investantrag->getId());
            foreach($daten_investantrag as $d){
                array_push($daten, $d->getBezeichnung(), $d->getMenge(), $d->getEp(), $d->getSumme());
            }
            foreach($best_investantrag as $b){
                $datum = $b->getDatum();
			    $datum = date_format($datum, 'd.m.Y');
                array_push($bestaetigungen, array('bestaetiger' => $b->getBestaetiger()->getName(), 'datum' => $datum, 'position' => $b->getPosition()));
            }
            $result = true;
        }else {
            $result = false;
        }

        die (json_encode(array("res" => $result, "daten" => $daten, "begruendung" => $investantrag->getBegruendung(), "best" => $bestaetigungen)));

    }

    /**
     * @Route("/bestinvestantrag", name="bestinvestantrag")
     */
    public function bestInvestantragAction(Request $request, ManageInvantraege $m, ManageBest $manageBest)
    {
        $id_investantrag = $_POST['id'];
        $em = $this->getDoctrine()->getManager();
        
        $benutzer = $em->getrepository(Benutzerverwaltung::class)->find($id_investantrag);
        $investantrag = $em->getRepository(Investantraege::class)->find($id_investantrag);
        $manageBest->setUser(128);

        if($manageBest->getStatus($investantrag)!=null){
            $manageBest->setPositionEBenutzer($benutzer, $investantrag->getKostenstelle());
            //antrag ansehen erlaubt
            if($manageBest->addBestInv($investantrag, $benutzer, $investantrag->getKostenstelle())){
                //antrag bestätigt
                if (!empty($manageBest->getEmailsBenutzer())) {
                    //email an nächsten senden
                    $betreff = "Investantrag zu bestätigen";
                    $nachricht = "Ein neuer Antrag #". $investantrag->getIdName() ." wurde gestellt und wartet auf Deine Bestätigung.<br><a href='intranet/repo-investimenti/pages/investitionsantraege.php?id=".$id_name."'>Zum Antrag</a>";
                    $header = 'Content-type: text/html; charset=UTF-8' . "\r\n";
                    $header .= 'From: PCS Investitionsplan-System' . "\r\n";
                    $m->sendEmail($manageBest->getEmailsBenutzer(), $betreff, $nachricht, $header);
                }else {
                    if($manageBest->getPosition()==5 && $investantrag->getAntragsteller()->getId()!=$benutzer->getId()){
                        //vom Geschäftsführer bestätigt und nicht antragsteller
                        //email an den antragsteller und als cc alle beteiligten
                        $cc_emails = array();
                        $benutzerKst = $manageBest->getBenutzerKst($investantrag->getKostenstelle());
                        if($investantrag->getAntragsteller()->getPosition()==4){
                            //antragsteller ist finanzleiter
                            //cc_array = geschäftsführer?
                        }else{
                            //antragsteller hat position < 4 (wo 4 = finanzleiter)
                            //cc_array = alle beteiligten?
                        }
                        //sendEmail();
                    }
                }
            }
            //$result = true;
        }else {
            //$result = false;
        }


    }

    /**
     * @Route("/addbenutzer", name="addbenutzer")
     */
    public function addBenutzerAction(Request $request, UserPasswordEncoderInterface $encoder)
    {

        $name=$_POST['name'];
        $benutzername=$_POST['benutzername'];
        $email=$_POST['email'];
        $position=$_POST['position'];
        $rechte=$_POST['benutzerrechte'];
        //$passwort_hash = password_hash("pcspcs", PASSWORD_DEFAULT);
        $user = new Benutzerverwaltung();
        $plainPassword = 'pcspcs';
        $passwort_hash = $encoder->encodePassword($user, $plainPassword);


        $em = $this->getDoctrine()->getManager();

        // sql query
        $sql = "INSERT INTO benutzerverwaltung (name, benutzername, email, position, passwort, benutzerrechte) 
                VALUES (:name, :benutzername, :email, :position, :passwort, :benutzerrechte)";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('name', $name);
        $stmt->bindValue('benutzername', $benutzername);
        $stmt->bindValue('email', $email);
        $stmt->bindValue('position', $position);
        $stmt->bindValue('passwort', $passwort_hash);
        $stmt->bindValue('benutzerrechte', $rechte);
        
        try {
            $result = $stmt->execute();
        } catch (\Exception $e) {
            $result = false;
        }

        if($result){
            $benutzer_id = $em->getConnection()->lastInsertId();
            $result = $benutzer_id;
        }
        
        die ($result);

        /*
        foreach($kostenstelle as $k) {
            echo($k->getId()->getName()."<br>");
            echo($k->getBezeichnung()."<br>");
            echo($k->setKstLeiter()->getName()."<br>");
            echo($k->setKstLeiter()->getName()."<br>");
            echo($k->getBereichsleiter()->getName()."<br>");
        }
        */

    }

    /**
     * @Route("/deletebenutzer", name="deletebenutzer")
     */
    public function deleteBenutzerAction(Request $request)
    {

        $id=$_POST['id'];

        $em = $this->getDoctrine()->getManager();

        $sql = "DELETE FROM benutzerverwaltung WHERE id = :id";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('id', $id);
        
        try {
            $result = $stmt->execute();
        } catch (\Exception $e) {
            $result = false;
        }

        die ($result);

    }

    /**
     * @Route("/editbenutzer", name="editbenutzer")
     */
    public function editBenutzerAction(Request $request)
    {

        $id=intval($_POST['id_edit']);
        $name=$_POST['name'];
        $benutzername=$_POST['benutzername'];
        $email=$_POST['email'];
        $position=$_POST['position'];
        $benutzerrechte=$_POST['benutzerrechte'];

        $em = $this->getDoctrine()->getManager();

        $sql = "UPDATE benutzerverwaltung SET name = :name, benutzername = :benutzername, email = :email, position = :position, benutzerrechte = :benutzerrechte
                WHERE benutzerverwaltung.id = :id";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('id', $id);
        $stmt->bindValue('name', $name);
        $stmt->bindValue('benutzername', $benutzername);
        $stmt->bindValue('email', $email);
        $stmt->bindValue('position', $position);
        $stmt->bindValue('benutzerrechte', $benutzerrechte);
        
        try {
            $result = $stmt->execute();
        } catch (\Exception $e) {
            $result = false;
        }

        die ($result);

    }

    /**
     * @Route("/resetpassword", name="resetpassword")
     */
    public function resetPasswordAction(Request $request)
    {

        $id = $_POST['id'];
        //$passwort_hash = password_hash("pcspcs", PASSWORD_DEFAULT);
        $user = new Benutzerverwaltung();
        $plainPassword = 'pcspcs';
        $passwort_hash = $encoder->encodePassword($user, $plainPassword);

        $em = $this->getDoctrine()->getManager();

        $sql = "UPDATE benutzerverwaltung SET passwort = :passwort
                WHERE benutzerverwaltung.id = :id";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('id', $id);
        $stmt->bindValue('passwort', $passwort_hash);
        
        try {
            $result = $stmt->execute();
        } catch (\Exception $e) {
            $result = false;
        }

        die ($result);

    }

    /**
     * @Route("/editpassword", name="editpassword")
     */
    public function editPasswordAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $passwortAlt = $_POST['passwortAlt'];
        $passwortNeu = trim($_POST['passwortNeu']);
        $passwortNeu2 = trim($_POST['passwortNeu2']);

        $user = new Benutzerverwaltung();
        $passwort_hash = $encoder->encodePassword($user, $passwortNeu);
        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $isPassValid = $encoder->isPasswordValid($usr, $passwortAlt);

        if($isPassValid){
            $em = $this->getDoctrine()->getManager();

            $sql = "UPDATE benutzerverwaltung SET passwort = :passwort
                    WHERE benutzerverwaltung.id = :id";
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->bindValue('id', $usr->getId());
            $stmt->bindValue('passwort', $passwort_hash);
            
            try {
                $result = $stmt->execute();
            } catch (\Exception $e) {
                $result = false;
            }
        }
        else {
            $result = false;
        }
        
        die ($result);

    }

    /**
     * @Route("/registrieren")
     */
    public function registrierenAction(UserPasswordEncoderInterface $encoder)
    {
        $user = new Benutzerverwaltung();
        $pass = $encoder->encodePassword($user,"pcspcs");
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $usr=$usr->getName();
        //return new Response('<html><body>'.$usr.'</body></html>');

        $token = $this->get('security.token_storage')->getToken();
        return new Response('<html><body>'.$token.'</body></html>');


        $name = $_POST['name'];
        $benutzername = $_POST['benutzername'];
        $email = $_POST['email'];
        $passw = $_POST['passw'];
        $user = new Benutzerverwaltung();
        $passwort_hash = $encoder->encodePassword($user, $passw);

        $em = $this->getDoctrine()->getManager();

        $sql = "SELECT * FROM benutzerverwaltung WHERE benutzerverwaltung.benutzername = :benutzername";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('benutzername', $benutzername);
        
        $stmt->execute();
        $stmt->store_result();

        if( $stmt->num_rows == 0){
            //register user
            $sql = "INSERT INTO benutzerverwaltung (name, benutzername, email, position, passwort, benutzerrechte) 
                VALUES (:name, :benutzername, :email, :position, :passwort, :benutzerrechte)";
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->bindValue('name', $name);
            $stmt->bindValue('benutzername', $benutzername);
            $stmt->bindValue('email', $email);
            $stmt->bindValue('position', '1');
            $stmt->bindValue('passwort', $passwort_hash);
            $stmt->bindValue('benutzerrechte', '1');

            try {
                $result = $stmt->execute();
            } catch (\Exception $e) {
                $result = false;
            }
        }else {
            $result = false;
        }

        die ($result);

    }

    /**
     * @Route("/sendemail/{name}")
     */
    public function sendemailAction($name)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Hello Email')
            ->setFrom('send@example.com')
            ->setTo('anuredini@pcs.de')
            ->setBody(
                $this->renderView(
                    'email.txt.twig',
                    array('name' => $name)
                )
            )
        ;
        $this->get('mailer')->send($message);

        return new Response('<html><body>'.$name.'</body></html>');
    }

}
