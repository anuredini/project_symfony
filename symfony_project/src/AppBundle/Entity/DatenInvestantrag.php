<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DatenInvestantrag
 *
 * @ORM\Table(name="daten_investantrag", indexes={@ORM\Index(name="daten_investantrag_ibfk_1", columns={"investantrag"})})
 * @ORM\Entity
 */
class DatenInvestantrag
{
    /**
     * @var string
     *
     * @ORM\Column(name="bezeichnung", type="string", length=60, nullable=false)
     */
    private $bezeichnung;

    /**
     * @var integer
     *
     * @ORM\Column(name="menge", type="integer")
     */
    private $menge;

    /**
     * @var float
     *
     * @ORM\Column(name="ep", type="float", precision=10, scale=0, nullable=false)
     */
    private $ep;

    /**
     * @var float
     *
     * @ORM\Column(name="summe", type="float", precision=10, scale=0, nullable=false)
     */
    private $summe;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Investantraege
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Investantraege")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="investantrag", referencedColumnName="id")
     * })
     */
    private $investantrag;

    /**
     * Get bezeichnung
     *
     * @return string
     */
    public function getBezeichnung()
    {
        return $this->bezeichnung;
    }

    /**
     * Get menge
     *
     * @return integer
     */
    public function getMenge()
    {
        return $this->menge;
    }

    /**
     * Get ep
     *
     * @return float
     */
    public function getEp()
    {
        return number_format((float)($this->ep), 2, '.', '');
    }

    /**
     * Get summe
     *
     * @return float
     */
    public function getSumme()
    {
        return number_format((float)($this->summe), 2, '.', '');;
    }            



}

