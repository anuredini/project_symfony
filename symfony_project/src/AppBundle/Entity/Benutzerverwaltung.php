<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Benutzerverwaltung
 *
 * @ORM\Table(name="benutzerverwaltung", indexes={@ORM\Index(name="benutzername", columns={"benutzername"}), @ORM\Index(name="benutzername_2", columns={"benutzername"})})
 * @ORM\Entity
 */
class Benutzerverwaltung implements UserInterface, \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="benutzername", type="string", length=50, nullable=false)
     */
    private $benutzername;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="passwort", type="string", length=250, nullable=false)
     */
    private $passwort;

    /**
     * @var integer
     *
     * @ORM\Column(name="benutzerrechte", type="integer", nullable=false)
     */
    private $benutzerrechte;

 public function getUsername()
    {
        return $this->benutzername;
    }

public function getPassword()
    {
        return $this->passwort;
    }
 public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }
public function getRoles()
    {
        if($this->getBenutzerrechte()==4){
            return array('ROLE_ADMIN');

        }else {
            return array('ROLE_USER');
        }
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->benutzername,
            $this->passwort,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->benutzername,
            $this->passwort,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }


    /**
     * Set id
     *
     * @param id $id
     *
     * @return Benutzerverwaltung
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set benutzername
     *
     * @param string $benutzername
     *
     * @return Benutzerverwaltung
     */
    public function setBenutzername($benutzername)
    {
        $this->benutzername = $benutzername;

        return $this;
    }

    /**
     * Get benutzername
     *
     * @return string
     */
    public function getBenutzername()
    {
        return $this->benutzername;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Benutzerverwaltung
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Benutzerverwaltung
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Benutzerverwaltung
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set passwort
     *
     * @param string $passwort
     *
     * @return Benutzerverwaltung
     */
    public function setPasswort($passwort)
    {
        $this->passwort = $passwort;

        return $this;
    }

    /**
     * Get passwort
     *
     * @return string
     */
    public function getPasswort()
    {
        return $this->passwort;
    }

    /**
     * Set benutzerrechte
     *
     * @param integer $benutzerrechte
     *
     * @return Benutzerverwaltung
     */
    public function setBenutzerrechte($benutzerrechte)
    {
        $this->benutzerrechte = $benutzerrechte;

        return $this;
    }

    /**
     * Get benutzerrechte
     *
     * @return integer
     */
    public function getBenutzerrechte()
    {
        return $this->benutzerrechte;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
