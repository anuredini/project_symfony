<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kostenstellenplan
 *
 * @ORM\Table(name="kostenstellenplan", indexes={@ORM\Index(name="kst_leiter", columns={"kst_leiter"}), @ORM\Index(name="stellvetreter", columns={"stellvertreter"}), @ORM\Index(name="bereichsleiter", columns={"bereichsleiter"})})
 * @ORM\Entity
 */
class Kostenstellenplan
{
    /**
     * @var string
     *
     * @ORM\Column(name="bezeichnung", type="text", length=65535, nullable=false)
     */
    private $bezeichnung;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Benutzerverwaltung
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Benutzerverwaltung")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bereichsleiter", referencedColumnName="id")
     * })
     */
    private $bereichsleiter;

    /**
     * @var \AppBundle\Entity\Benutzerverwaltung
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Benutzerverwaltung")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stellvertreter", referencedColumnName="id")
     * })
     */
    private $stellvertreter;

    /**
     * @var \AppBundle\Entity\Benutzerverwaltung
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Benutzerverwaltung")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kst_leiter", referencedColumnName="id")
     * })
     */
    private $kstLeiter;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get kstLeiter
     *
     * @return \AppBundle\Entity\Benutzerverwaltung
     */
    public function getKstLeiter()
    {
        return $this->kstLeiter;
    }

    /**
     * Get stellvertreter
     *
     * @return \AppBundle\Entity\Benutzerverwaltung
     */
    public function getStellvertreter()
    {
        return $this->stellvertreter;
    }

    /**
     * Get bereichsleiter
     *
     * @return \AppBundle\Entity\Benutzerverwaltung
     */
    public function getBereichsleiter()
    {
        return $this->bereichsleiter;
    }

    /**
     * Get bezeichnung
     *
     * @return string
     */
    public function getBezeichnung()
    {
        return $this->bezeichnung;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return id
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set bezeichnung
     *
     * @param string $bezeichnung
     *
     * @return Kostenstellenplan
     */
    public function setBezeichnung($bezeichnung)
    {
        $this->bezeichnung = $bezeichnung;

        return $this;
    }

    /**
     * Set bereichsleiter
     *
     * @param \AppBundle\Entity\Benutzerverwaltung $bereichsleiter
     *
     * @return Kostenstellenplan
     */
    public function setBereichsleiter(\AppBundle\Entity\Benutzerverwaltung $bereichsleiter = null)
    {
        $this->bereichsleiter = $bereichsleiter;

        return $this;
    }

    /**
     * Set stellvertreter
     *
     * @param \AppBundle\Entity\Benutzerverwaltung $stellvertreter
     *
     * @return Kostenstellenplan
     */
    public function setStellvertreter(\AppBundle\Entity\Benutzerverwaltung $stellvertreter = null)
    {
        $this->stellvertreter = $stellvertreter;

        return $this;
    }

    /**
     * Set kstLeiter
     *
     * @param \AppBundle\Entity\Benutzerverwaltung $kstLeiter
     *
     * @return Kostenstellenplan
     */
    public function setKstLeiter(\AppBundle\Entity\Benutzerverwaltung $kstLeiter = null)
    {
        $this->kstLeiter = $kstLeiter;

        return $this;
    }
}
