<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Investitionsplanung
 *
 * @ORM\Table(name="investitionsplanung", indexes={@ORM\Index(name="bearbeiter", columns={"bearbeiter"}), @ORM\Index(name="id_kostenstelle", columns={"kostenstelle"})})
 * @ORM\Entity
 */
class Investitionsplanung
{
    /**
     * @var integer
     *
     * @ORM\Column(name="anzahl", type="integer", nullable=false)
     */
    private $anzahl;

    /**
     * @var float
     *
     * @ORM\Column(name="preis", type="float", precision=10, scale=0, nullable=false)
     */
    private $preis;

    /**
     * @var string
     *
     * @ORM\Column(name="beschreibung", type="string", length=300, nullable=false)
     */
    private $beschreibung;

    /**
     * @var float
     *
     * @ORM\Column(name="anschaffungskosten", type="float", precision=10, scale=0, nullable=false)
     */
    private $anschaffungskosten;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="zeitpunkt", type="date", nullable=false)
     */
    private $zeitpunkt;

    /**
     * @var string
     *
     * @ORM\Column(name="begruendung", type="string", length=300, nullable=false)
     */
    private $begruendung;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Benutzerverwaltung
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Benutzerverwaltung")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bearbeiter", referencedColumnName="id")
     * })
     */
    private $bearbeiter;

    /**
     * @var \AppBundle\Entity\Kostenstellenplan
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Kostenstellenplan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kostenstelle", referencedColumnName="id")
     * })
     */
    private $kostenstelle;


}

