<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Investantraege
 *
 * @ORM\Table(name="investantraege", indexes={@ORM\Index(name="kostenstelle_id", columns={"kostenstelle"}), @ORM\Index(name="benutzer_id", columns={"antragsteller"})})
 * @ORM\Entity
 */
class Investantraege
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="id_name", type="string", length=15, nullable=false)
     */
    private $idName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="wunschtermin", type="date", nullable=false)
     */
    private $wunschtermin;

    /**
     * @var integer
     *
     * @ORM\Column(name="budget", type="integer", nullable=false)
     */
    private $budget;

    /**
     * @var float
     *
     * @ORM\Column(name="gesamt", type="float", precision=10, scale=0, nullable=true)
     */
    private $gesamt;

    /**
     * @var string
     *
     * @ORM\Column(name="begruendung", type="string", length=300, nullable=false)
     */
    private $begruendung;

    /**
     * @var \Kostenstellenplan
     *
     * @ORM\ManyToOne(targetEntity="Kostenstellenplan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kostenstelle", referencedColumnName="id")
     * })
     */
    private $kostenstelle;

    /**
     * @var \Benutzerverwaltung
     *
     * @ORM\ManyToOne(targetEntity="Benutzerverwaltung")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="antragsteller", referencedColumnName="id")
     * })
     */
    private $antragsteller;



    /**
     * Set idName
     *
     * @param string $idName
     *
     * @return Investantraege
     */
    public function setIdName($idName)
    {
        $this->idName = $idName;

        return $this;
    }

    /**
     * Get idName
     *
     * @return string
     */
    public function getIdName()
    {
        return $this->idName;
    }

    /**
     * Set wunschtermin
     *
     * @param \DateTime $wunschtermin
     *
     * @return Investantraege
     */
    public function setWunschtermin($wunschtermin)
    {
        $this->wunschtermin = $wunschtermin;

        return $this;
    }

    /**
     * Get wunschtermin
     *
     * @return \DateTime
     */
    public function getWunschtermin()
    {
        return $this->wunschtermin;
    }

    /**
     * Set budget
     *
     * @param integer $budget
     *
     * @return Investantraege
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return integer
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set gesamt
     *
     * @param float $gesamt
     *
     * @return Investantraege
     */
    public function setGesamt($gesamt)
    {
        $this->gesamt = $gesamt;

        return $this;
    }

    /**
     * Get gesamt
     *
     * @return float
     */
    public function getGesamt()
    {
        return $this->gesamt;
    }

    /**
     * Set begruendung
     *
     * @param string $begruendung
     *
     * @return Investantraege
     */
    public function setBegruendung($begruendung)
    {
        $this->begruendung = $begruendung;

        return $this;
    }

    /**
     * Get begruendung
     *
     * @return string
     */
    public function getBegruendung()
    {
        return $this->begruendung;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set antragsteller
     *
     * @param \AppBundle\Entity\Benutzerverwaltung $antragsteller
     *
     * @return Investantraege
     */
    public function setAntragsteller(\AppBundle\Entity\Benutzerverwaltung $antragsteller = null)
    {
        $this->antragsteller = $antragsteller;

        return $this;
    }

    /**
     * Get antragsteller
     *
     * @return \AppBundle\Entity\Benutzerverwaltung
     */
    public function getAntragsteller()
    {
        return $this->antragsteller;
    }

    /**
     * Set kostenstelle
     *
     * @param \AppBundle\Entity\Kostenstellenplan $kostenstelle
     *
     * @return Investantraege
     */
    public function setKostenstelle(\AppBundle\Entity\Kostenstellenplan $kostenstelle = null)
    {
        $this->kostenstelle = $kostenstelle;

        return $this;
    }

    /**
     * Get kostenstelle
     *
     * @return \AppBundle\Entity\Kostenstellenplan
     */
    public function getKostenstelle()
    {
        return $this->kostenstelle;
    }

    public function getGesamtFormat()
    {
        return number_format((float)($this->gesamt), 2, '.', '');
    }
    

}
