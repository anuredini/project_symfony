<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BestInvestantrag
 *
 * @ORM\Table(name="best_investantrag", indexes={@ORM\Index(name="id_bestaetiger", columns={"bestaetiger"}), @ORM\Index(name="id_investantrag", columns={"investantrag"})})
 * @ORM\Entity
 */
class BestInvestantrag
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datum", type="date", nullable=false)
     */
    private $datum;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Investantraege
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Investantraege")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="investantrag", referencedColumnName="id")
     * })
     */
    private $investantrag;

    /**
     * @var \AppBundle\Entity\Benutzerverwaltung
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Benutzerverwaltung")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bestaetiger", referencedColumnName="id")
     * })
     */
    private $bestaetiger;


    /**
     * Set datum
     *
     * @param \DateTime $datum
     *
     * @return BestInvestantrag
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;

        return $this;
    }

    /**
     * Get datum
     *
     * @return \DateTime
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return BestInvestantrag
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set investantrag
     *
     * @param \AppBundle\Entity\Investantraege $investantrag
     *
     * @return BestInvestantrag
     */
    public function setInvestantrag(\AppBundle\Entity\Investantraege $investantrag = null)
    {
        $this->investantrag = $investantrag;

        return $this;
    }

    /**
     * Get investantrag
     *
     * @return \AppBundle\Entity\Investantraege
     */
    public function getInvestantrag()
    {
        return $this->investantrag;
    }

    /**
     * Set bestaetiger
     *
     * @param \AppBundle\Entity\Benutzerverwaltung $bestaetiger
     *
     * @return BestInvestantrag
     */
    public function setBestaetiger(\AppBundle\Entity\Benutzerverwaltung $bestaetiger = null)
    {
        $this->bestaetiger = $bestaetiger;

        return $this;
    }

    /**
     * Get bestaetiger
     *
     * @return \AppBundle\Entity\Benutzerverwaltung
     */
    public function getBestaetiger()
    {
        return $this->bestaetiger;
    }

    /**
     *
     * 
     * 
     */
    public function getMaxValue()
    {
        //return $this->findOneByInvestantrag($inv_antrag);
        return $this;
    }    
   

}
