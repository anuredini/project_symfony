/* Create an array with the values of all the input boxes in a column */
$.fn.dataTable.ext.order['dom-text'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).val();
    } );
}
 
/* Create an array with the values of all the input boxes in a column, parsed as numbers */
$.fn.dataTable.ext.order['dom-text-numeric'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).val() * 1;
    } );
}
 
/* Create an array with the values of all the select options in a column */
$.fn.dataTable.ext.order['dom-select'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('select', td).val();
    } );
}
 
/* Create an array with the values of all the checkboxes in a column */
$.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).prop('checked') ? '1' : '0';
    } );
}

/* Initialise the table with the required column ordering data types */
$(document).ready(function() {

    //init your datatable
    $('#myTable').DataTable( {
			"language": {
				 "sEmptyTable":      "Keine Daten in der Tabelle vorhanden",
			"sInfo":            "_START_ bis _END_ von _TOTAL_ Einträgen",
			"sInfoEmpty":       "0 bis 0 von 0 Einträgen",
			"sInfoFiltered":    "(gefiltert von _MAX_ Einträgen)",
			"sInfoPostFix":     "",
			"sInfoThousands":   ".",
			"sLengthMenu":      "_MENU_ Einträge anzeigen",
			"sLoadingRecords":  "Wird geladen...",
			"sProcessing":      "Bitte warten...",
			"sSearch":          "Suchen",
			"sZeroRecords":     "Keine Einträge vorhanden.",
			"oPaginate": {
				"sFirst":       "Erste",
				"sPrevious":    "Zurück",
				"sNext":        "Nächste",
				"sLast":        "Letzte"
			},
			"oAria": {
				"sSortAscending":  ": aktivieren, um Spalte aufsteigend zu sortieren",
				"sSortDescending": ": aktivieren, um Spalte absteigend zu sortieren"
			}
			},
			
        "columns": [ //all column type here. null if is normal text
            { "orderDataType": "dom-dom-text-numeric" },
            null,
            null,
			null,
			null,
            { "orderable": false }
            /*
            { "orderDataType": "dom-text", type: 'string' },
            { "orderDataType": "dom-text-numeric" },
            { "orderDataType": "dom-select" },
            { "orderDataType": "dom-checkbox" }
            */
        ]
    } );

    /*
    _______select data example_______
        console.log($('input', '#myDataTables') );
                        ========     
        console.log($('#myDataTables input '));
    */

  //_______add listener at array example_______

//take object with id "myDataTables" and are input tag  -> // all the input of the table
    //$('#myDataTables input')
//take the table -> than the second column -> than all input
    //$('#myDataTables td:nth-child(1) input').on( 'keyup', function(e)


} );



